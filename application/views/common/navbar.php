<nav class="navbar navbar-expand-lg navbar-dark primary-color">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">Controle Financeiro</a>
        <button class="navbar-toggler" type="button" data-mdb-toggle="collapse" 
        data-mdb-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="<?= base_url('home') ?>">Home</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" 
                    data-mdb-toggle="dropdown" aria-expanded="false">
                        Cadastro
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <li>
                            <a class="dropdown-item" href="<?= base_url('usuario/cadastro') ?>">Usuário</a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="#">Conta Bancária</a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="#">Parceiros</a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-mdb-toggle="dropdown" aria-expanded="false">
                        Lançamentos
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <li>
                            <a class="dropdown-item" href="#">Contas a Pagar</a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="#">Contas a Receber</a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="#">Fluxo de Caixa</a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-mdb-toggle="dropdown" aria-expanded="false">
                        Relatórios
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <li>
                            <a class="dropdown-item" href="#">Lançamento por Período</a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="#">Resumo Mensal</a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="#">Resumo Anual</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>