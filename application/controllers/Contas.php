<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Contas extends MY_Controller
{

    public function index()
    {
        echo 'Lista de todas as contas';
    }

    public function pagar($mes = 0, $ano = 0)
    {
        // Salva os dados da nova conta caso exista
        $this->load->model('ContasModel', 'conta');
        $this->conta->cria('pagar');

        // Eecupera a lista de contas a pagar
        $v['lista'] = $this->conta->lista('pagar');
        $v['tipo'] = 'pagar';

        // carrega a view e passa a lista de contas
        $html = $this->load->view('contas/lista_contas', $v, true);

        // Exibe esta view 
        $this->show($html);
    }

    public function receber($mes = 0, $ano = 0)
    {
    }
}
